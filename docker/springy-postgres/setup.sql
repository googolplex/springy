create user springy password 'springy' login;
create database springy owner springy encoding 'utf8';
grant all privileges on database springy to springy;

\c springy springy

create table users(
    id bigserial primary key,
    name varchar(32) not null,
    password varchar(128) not null,
    email varchar(128) not null
);
insert into users (name, password, email) values ('admin', 'admin', 'admin@example.com');
