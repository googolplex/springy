#!/bin/bash

pushd docker/tomcat
docker build -t netvl/tomcat:8-jre8 .
popd

pushd docker/springy-postgres
docker build -t netvl/springy-postgres .
popd
