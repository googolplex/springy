<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=utf-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="static/styles.css">
</head>
<body>
<%--@elvariable id="errMessage" type="java.lang.String"--%>
<c:if test="${errMessage ne null}">
    <p class="err-message">${errMessage}</p>
</c:if>

<form action="<c:url value="/login"/>" method="post">
    <%--@elvariable id="previousLogin" type="java.lang.String"--%>
    <p><label>Login:<br><input type="text" name="login" value="${previousLogin}"></label></p>
    <p><label>Password:<br><input type="password" name="password"></label></p>
    <p><input type="submit" name="submit" value="login"></p>
</form>
</body>
</html>
