<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=utf-8" language="java" %>
<html>
<head>
    <title>Users</title>
    <script>
        function deleteUser(id) {
            document.forms["delete_" + id].submit();
        }
    </script>
    <link rel="stylesheet" type="text/css" href="static/styles.css">
</head>
<body>

<%--@elvariable id="currentUser" type="java.lang.String"--%>
<p>Hello, ${currentUser}!&nbsp;<a href="<c:url value="/logout"/>">logout</a></p>

<%--@elvariable id="okMessage" type="java.lang.String"--%>
<%--@elvariable id="errMessage" type="java.lang.String"--%>
<c:if test="${okMessage ne null}">
    <p class="ok-message">${okMessage}</p>
</c:if>
<c:if test="${errMessage ne null}">
    <p class="err-message">${errMessage}</p>
</c:if>

<p></p>

<table class="users-table">
    <tr>
        <th>ID#</th>
        <th>Name</th>
        <th>Email</th>
        <th>Actions</th>
    </tr>
<%--@elvariable id="users" type="java.util.List<springy.model.User>"--%>
<c:forEach items="${users}" var="user">
    <tr>
        <td>${user.id}</td>
        <td>${user.name}</td>
        <td>${user.email}</td>
        <td>
            <form name="delete_${user.id}" action="<c:url value="/users/delete/${user.id}"/>" method="post" style="display: none;">
                <input type="hidden" name="name" value="${user.name}">
            </form>
            <nobr>
                <a href="<c:url value="/users/edit/${user.id}"/>">edit</a>
                <a onclick="deleteUser(${user.id})" href="#">delete</a>
            </nobr>
        </td>
    </tr>
</c:forEach>
</table>
<p><a href="<c:url value="/users/add"/>">add</a></p>
</body>
</html>
