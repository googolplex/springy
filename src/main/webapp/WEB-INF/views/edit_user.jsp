<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--@elvariable id="user" type="springy.model.User"--%>
<html>
<head>
    <title>
        <c:choose>
            <c:when test="${user ne null}">Edit user ${user.name}</c:when>
            <c:otherwise>Create new user</c:otherwise>
        </c:choose>
    </title>
</head>
<body>
<c:choose>
    <c:when test="${user ne null}"><c:url var="url" value="/users/edit/${user.id}"/></c:when>
    <c:otherwise><c:url var="url" value="/users/add" /></c:otherwise>
</c:choose>
<form action="${url}" method="post">
    <c:if test="${user ne null}"><input type="hidden" name="id" value="${user.id}"></c:if>
    <p><label>Name<br><input type="text" name="name" value="${user.name}"></label></p>
    <p><label>Password<br><input type="text" name="password" value="${user.password}"></label></p>
    <p><label>Email<br><input type="text" name="email" value="${user.email}"></label></p>
    <p><input type="submit" name="submit" value="save">&nbsp;<a href="<c:url value="/users" />">cancel</a></p>
</form>
</body>
</html>
