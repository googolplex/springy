package springy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springy.services.UserService;
import springy.services.sql.SqlUserService;

// This is a configuration class for the root Spring context. This class contains bean definitions global to
// the whole web application. Usually this class contains DAOs, some global business-logic, global state holders etc.
// In this particular case we will have DAO classes and database configuration here.
@Configuration  // This annotation marks this class as a suitable Spring Java configuration class
@Import({DatabaseConfig.class})  // This annotation merges another configuration class with this one
public class RootConfig {
    @Bean
    public UserService userService() {
        return new SqlUserService();
    }
}
