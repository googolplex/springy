package springy.config;

import org.springframework.context.annotation.*;
import org.springframework.web.servlet.config.annotation.*;
import springy.interceptors.LoginInterceptor;
import springy.model.SessionUser;

// This is a configuration class for the application context. The application context contains beans local to
// the particular dispatcher servlet - controllers, resource handlers, view resolvers, interceptors etc.
@Configuration  // enable using this class as a configuration source for the context
@ComponentScan("springy.web")  // scan "springy.web" package for annotated beans (controllers in our case)
@EnableWebMvc  // mark this configuration class as a configuration for WEB MVC application
               // this annotation imports various important beans necessary for WEB MVC functioning
// This class extends WebMvcConfigurerAdapter class which provides several methods to configure WEB MVC application
// behavior. The used methods are marked by @Override annotation below.
public class AppConfig extends WebMvcConfigurerAdapter {
    // Here we configure view resolvers, that is, we tell the dispatcher servlet where to find our views
    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        // We're using JSP views located in /WEB-INF/views/ directory in our web application, and we also specify
        // that ".jsp" suffix should automatically be appended so we can name our views as "some_view", not
        // "some_view.jsp"
        registry.jsp("/WEB-INF/views/", ".jsp");
    }

    // Here we configure static resource handlers, that is, we specify which files should be passed back to the
    // client without any processing through controllers. This is useful for static resources like Javascript,
    // CSS styles or images.
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // All requests to URLs like "/static/something.css" should be served by files located
        // in /static/ directory in our web application.
        registry.addResourceHandler("/static/**").addResourceLocations("/static/");
    }

    // Here we configure interceptors, that is, special classes which will be called before or after processing
    // each request. This functionality is useful to add some code necessary for all requests, for example,
    // we can check that the user is logged in and if she is not, redirect her to the login page.
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // Register the only interceptor for all known URL paths except /login (we don't need to check
        // the current user for login page)
        // The interceptor itself is obtained from the context by calling another method in this configuration class.
        // That's how dependencies between beans are resolved in Java-based configuration.
        registry.addInterceptor(loginInterceptor()).addPathPatterns("/**").excludePathPatterns("/login");
    }

    // Declare the login interceptor bean (used in the method above).
    // Because this is just like any other bean, it is subject for all Spring niceties, like dependency injection.
    @Bean
    public LoginInterceptor loginInterceptor() {
        return new LoginInterceptor();
    }

    // Declare a special session user bean and put it into session scope.
    // This bean will contain information about the current user when she logs in. This is how authentication
    // is implemented. For example, login interceptor configured above uses this bean to check whether the user
    // is logged in.
    @Bean
    // @Scope annotation configures scope for the bean. Here we use session scope and we also specify how
    // this bean should be proxied. Proxies are very important when working with scoped beans but this is not the
    // place to describe why they are needed.
    @Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public SessionUser sessionUser() {
        return new SessionUser();
    }
}
