package springy.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

// Here we configure database access beans
@Configuration
public class DatabaseConfig {
    // Here we create configuration for HikariCP, a JDBC connection pool
    @Bean
    public HikariConfig hikariConfig() {
        HikariConfig config = new HikariConfig();
        // The following line means that we will be using PostgreSQL JDBC driver
        config.setDataSourceClassName("org.postgresql.ds.PGSimpleDataSource");
        // Below lines are self-descripting
        config.setUsername("springy");
        config.setPassword("springy");
        config.addDataSourceProperty("serverName", "postgres");
        config.addDataSourceProperty("portNumber", 5432);
        config.addDataSourceProperty("databaseName", "springy");
        return config;
    }

    // And here we create the connection pool itself
    @Bean
    public DataSource dataSource() {
        return new HikariDataSource(hikariConfig());
    }
}
