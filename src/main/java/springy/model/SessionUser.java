package springy.model;

import java.io.Serializable;

public class SessionUser implements Serializable {
    private long id;
    private String name;
    private boolean login;

    public void setFromUser(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.login = true;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isLogin() {
        return login;
    }

    public void setLogin(boolean login) {
        this.login = login;
    }
}
