package springy.interceptors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import springy.model.SessionUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// This is login interceptor class which is configured in AppConfig. This class will be used to preprocess
// all incoming requests and check that the user is logged in. If she is not, then we need to redirect the user
// to the login page.
public class LoginInterceptor extends HandlerInterceptorAdapter {
    @Autowired SessionUser sessionUser;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!sessionUser.isLogin()) {
            response.sendRedirect(request.getContextPath() + "/login");
            return false;
        }
        return true;
    }
}
