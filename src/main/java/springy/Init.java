package springy;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;
import springy.config.AppConfig;
import springy.config.RootConfig;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

// An instance of class will be created just after the web application is deployed, and its onStartup method
// will then be called
public class Init implements WebApplicationInitializer {
    // This method receives an instance of ServletContext class which belongs JavaEE Servlets API and
    // can be used to configure the web application, akin to web.xml but in programmatic form.
    // With ServletContext it is possible to add listeners, servlets and filters and configure them all.
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        // Here we create two Spring contexts - a root context which is global for the whole application
        // and an application context local to one particular Spring dispatcher servlet.
        // Both these contexts will use Java-based configuration instead of the old XML-based one,
        // this is achieved by using AnnotationConfigWebApplicationContext class.
        // Spring establishes parent-child relationship between root context and application context automatically,
        // we don't have to do anything here.

        // Create root context and set its configuration class
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
        rootContext.register(RootConfig.class);

        // Add a servlet context listener whilch will hold our root application context and make it available
        // for the child context
        servletContext.addListener(new ContextLoaderListener(rootContext));

        // Create application context and set its configuration class
        AnnotationConfigWebApplicationContext appContext = new AnnotationConfigWebApplicationContext();
        appContext.register(AppConfig.class);

        // Create our only Spring dispatcher servlet which will contain MVC controllers
        ServletRegistration.Dynamic appServlet = servletContext.addServlet(
            "springyApp", new DispatcherServlet(appContext)
        );
        appServlet.setLoadOnStartup(1);  // the servlet should be started immediately after deploy
        appServlet.addMapping("/");      // the servlet should handle all requests

        // Create a Spring encoding filter which will force UTF-8 encoding on all requests and responses
        FilterRegistration.Dynamic encodingFilter = servletContext.addFilter(
            "encodingFilter", new CharacterEncodingFilter()
        );
        encodingFilter.setInitParameter("encoding", "utf-8");       // encoding is UTF-8
        encodingFilter.setInitParameter("forceEncoding", "true");   // this encoding should be enforced
        encodingFilter.addMappingForUrlPatterns(null, true, "/*");  // this filter must be applied to all requests
    }
}
