package springy.services.inmem;

import springy.model.User;
import springy.services.UserService;

import java.util.*;

public class InMemoryUserService implements UserService {
    private final Map<Long, User> users = new HashMap<>(Collections.singletonMap(
        0L, new User(0L, "admin", "admin", "admin@example.com")
    ));

    @Override
    public List<User> listUsers() {
        return Collections.unmodifiableList(new ArrayList<>(users.values()));
    }

    @Override
    public Optional<User> findById(long id) {
        return Optional.ofNullable(users.get(id));
    }

    @Override
    public Optional<User> findByLogin(String login) {
        return users.values().stream().filter(user -> user.getName().equals(login)).findFirst();
    }

    @Override
    public void insertOrUpdate(User user) {
        if (user.getId() == null) {
            long newId = users.values().stream().mapToLong(User::getId).max().orElse(-1) + 1;
            user.setId(newId);
            users.put(newId, user);
        } else {
            users.merge(user.getId(), user, (oldUser, newUser) -> newUser);
        }
    }

    @Override
    public boolean deleteById(long id) {
        return users.remove(id) != null;
    }
}
