package springy.services.sql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import springy.model.User;
import springy.services.UserService;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

@Component
public class SqlUserService implements UserService {
    @Autowired DataSource dataSource;

    private User fromResultSet(ResultSet resultSet) throws SQLException {
        return new User(
            resultSet.getLong("id"),
            resultSet.getString("name"),
            resultSet.getString("password"),
            resultSet.getString("email")
        );
    }

    private <T> T fromResultSetMulti(ResultSet resultSet, Supplier<T> initial, BiConsumer<T, User> appender) throws SQLException {
        T result = initial.get();
        while (resultSet.next()) {
            appender.accept(result, fromResultSet(resultSet));
        }
        return result;
    }

    @Override
    public List<User> listUsers() {
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM users")) {
            return Collections.unmodifiableList(fromResultSetMulti(resultSet, ArrayList::new, ArrayList::add));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<User> findById(long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE id = ?")) {
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(fromResultSet(resultSet));
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<User> findByLogin(String login) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE name = ?")) {
            statement.setString(1, login);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(fromResultSet(resultSet));
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void insertOrUpdate(User user) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                 user.getId() == null ?
                 "INSERT INTO users (name, password, email) VALUES (?, ?, ?)" :
                 "UPDATE users SET name = ?, password = ?, email = ? WHERE id = ?",
                 Statement.RETURN_GENERATED_KEYS
             )) {
            statement.setString(1, user.getName());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getEmail());
            if (user.getId() != null) {
                statement.setLong(4, user.getId());
            }

            statement.execute();

            if (user.getId() == null) {
                try (ResultSet resultSet = statement.getGeneratedKeys()) {
                    if (resultSet.next()) {
                        user.setId(resultSet.getLong("id"));
                    }
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean deleteById(long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement("DELETE FROM users WHERE id = ?")) {
            statement.setLong(1, id);
            statement.execute();
            return statement.getUpdateCount() > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
