package springy.services;

import springy.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> listUsers();
    Optional<User> findById(long id);
    Optional<User> findByLogin(String login);
    void insertOrUpdate(User user);
    boolean deleteById(long id);
}
