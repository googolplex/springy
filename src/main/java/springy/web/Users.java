package springy.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import springy.model.SessionUser;
import springy.model.User;
import springy.services.UserService;

import java.util.Optional;

@Controller
@RequestMapping("/users")
public class Users {
    @Autowired UserService userService;
    @Autowired SessionUser sessionUser;

    @RequestMapping(method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("users", userService.listUsers());
        model.addAttribute("currentUser", sessionUser.getName());
        return "list_users";
    }

    @RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable("id") long id, Model model, RedirectAttributes redirectAttributes) {
        Optional<User> user = userService.findById(id);
        if (user.isPresent()) {
            model.addAttribute("user", user.get());
            return "edit_user";
        } else {
            redirectAttributes.addFlashAttribute(Constants.ERR_MESSAGE, "User " + id + " can't be found");
            return "redirect:/users";
        }
    }

    @RequestMapping(value = "edit/{id}", method = RequestMethod.POST)
    public String doEdit(@PathVariable("id") long id,
                         @RequestParam("name") String name,
                         @RequestParam("password") String password,
                         @RequestParam("email") String email,
                         RedirectAttributes redirectAttributes) {
        User user = new User(id, name, password, email);
        userService.insertOrUpdate(user);
        redirectAttributes.addFlashAttribute(Constants.OK_MESSAGE, "User " + name + " has been updated");
        return "redirect:/users";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add() {
        return "edit_user";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String doAdd(@RequestParam("name") String name,
                        @RequestParam("password") String password,
                        @RequestParam("email") String email,
                        RedirectAttributes redirectAttributes) {
        User user = new User(null, name, password, email);
        userService.insertOrUpdate(user);
        redirectAttributes.addFlashAttribute(Constants.OK_MESSAGE, "Added user " + name);
        return "redirect:/users";
    }

    @RequestMapping(value = "delete/{id}", method = RequestMethod.POST)
    public String delete(@PathVariable("id") long id,
                         @RequestParam("name") String name,
                         RedirectAttributes redirectAttributes) {
        if (id == sessionUser.getId()) {
            redirectAttributes.addFlashAttribute(Constants.ERR_MESSAGE, "Cannot delete current user");
        } else if (userService.deleteById(id)) {
            redirectAttributes.addFlashAttribute(Constants.OK_MESSAGE, "User " + name + " has been deleted");
        } else {
            redirectAttributes.addFlashAttribute(Constants.ERR_MESSAGE, "User " + name + " can't be deleted");
        }
        return "redirect:/users";
    }
}
