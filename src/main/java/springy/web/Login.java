package springy.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import springy.model.SessionUser;
import springy.services.UserService;

@Controller
@RequestMapping("/login")
public class Login {
    @Autowired UserService userService;
    @Autowired SessionUser sessionUser;

    @RequestMapping(method = RequestMethod.GET)
    public String login() {
        return "login";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String doLogin(@RequestParam("login") String login,
                          @RequestParam("password") String password,
                          Model model,
                          RedirectAttributes redirectAttributes) {
        return userService.findByLogin(login)
            .filter(user -> user.getPassword().equals(password))
            .map(user -> {
                sessionUser.setFromUser(user);
                redirectAttributes.addFlashAttribute(Constants.OK_MESSAGE, "Login successful");
                return "redirect:/users";
            })
            .orElseGet(() -> {
                model.addAttribute(Constants.ERR_MESSAGE, "Invalid login or password");
                model.addAttribute("previousLogin", login);
                return "login";
            });
    }
}
