package springy.web;

public final class Constants {
    private Constants() {
    }

    public static final String OK_MESSAGE = "okMessage";
    public static final String ERR_MESSAGE = "errMessage";
}
