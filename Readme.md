To install docker images run:

```
$ bash ./init-docker.sh
```

Then to control services, use `docker-compose`:

```
$ cd docker/springy
$ docker-compose up
```

Tomcat container has its debug port exposed on port 5005, so it is possible to attach a remote debugger to it
to debug the application. HTTP port is exposed on 8080.

To deploy the application to tomcat, run the system as described above, update Docker host in `build.gradle`:

```
cargo {
    remote {
        hostname = "192.168.99.100"
    }
}
```

and use `cargoRedeployRemote` command:

```
$ ./gradlew :cargoRedeployRemote
```

Make sure you're using Java 8.
